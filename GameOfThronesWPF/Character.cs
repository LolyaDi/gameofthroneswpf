﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GameOfThronesWPF
{
    public class Character
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("culture")]
        public string Culture { get; set; }
        
        [JsonProperty("alive")]
        public bool Alive { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("placeOfBirth")]
        public string PlaceOfBirth { get; set; }

        [JsonProperty("father")]
        public string Father { get; set; }

        [JsonProperty("mother")]
        public string Mother { get; set; }

        [JsonProperty("spouse")]
        public ICollection<string> Spouses { get; set; }

        [JsonProperty("children")]
        public ICollection<string> Children { get; set; }

        [JsonProperty("allegiance")]
        public ICollection<string> Allegiances { get; set; }
    }
}
