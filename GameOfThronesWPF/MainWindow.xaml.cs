﻿using MaterialDesignThemes.Wpf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace GameOfThronesWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string DEFAULT_URL = "https://api.got.show/api/book/characters";

        private List<Character> _characters;

        public MainWindow()
        {
            InitializeComponent();

            _characters = new List<Character>();
            _characters.AddRange(GetCharacters(DEFAULT_URL));

            foreach (var character in _characters)
            {
                charactersTreeView.Items.Add(character.Name);
            }
        }
        
        private List<Character> GetCharacters(string url)
        {
            using (var client = new WebClient())
            {
                return JsonConvert.DeserializeObject<List<Character>>(client.DownloadString(url));
            }
        }

        private void CharactersTreeViewSelected(object sender, RoutedEventArgs e)
        {
            var selectedCharacter = _characters.Find(character => character.Name == ((TreeViewItem)e.OriginalSource).Header.ToString());

            try
            {
                characterImage.Source = new BitmapImage(new Uri(selectedCharacter.Image));
            }
            catch (ArgumentNullException)
            {
                characterImage.Source = new BitmapImage(new Uri("https://www.upsbs.org.ua/images/uploads/015e96e6a653950ded808f5704c0727f.jpg"));
            }

            characterGender.Kind = 
                  selectedCharacter.Gender == "male" ? PackIconKind.GenderMale
                : selectedCharacter.Gender == "female" ? PackIconKind.GenderFemale 
                : PackIconKind.GenderMaleFemale;

            characterName.Text = selectedCharacter.Name;
            characterAlive.Text = (selectedCharacter.Alive == true)? "Yes": "No";
            characterCulture.Text = selectedCharacter.Culture;
            characterPlaceOfBirth.Text = selectedCharacter.PlaceOfBirth;
            characterFather.Text = selectedCharacter.Father;
            characterMother.Text = selectedCharacter.Mother;

            characterSpouses.Text = string.Join(",", selectedCharacter.Spouses);
            characterChildren.Text = string.Join(",", selectedCharacter.Children);
            characterAllegiances.Text = string.Join(",", selectedCharacter.Allegiances);

            dialog.IsOpen = true;
        }

        private void SearchNameBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            foreach (var item in charactersTreeView.Items)
            {
               ((TreeViewItem)charactersTreeView.ItemContainerGenerator.ContainerFromItem(item)).Visibility = Visibility.Collapsed;
            }

            if (((TextBox)sender).Text == string.Empty)
            {
                foreach (var item in charactersTreeView.Items)
                {
                    ((TreeViewItem)charactersTreeView.ItemContainerGenerator.ContainerFromItem(item)).Visibility = Visibility.Visible;
                }
            }
            else
            {
                foreach (var item in charactersTreeView.Items)
                {
                    if (item.ToString().Contains(((TextBox)sender).Text))
                    {
                        ((TreeViewItem)charactersTreeView.ItemContainerGenerator.ContainerFromItem(item)).Visibility = Visibility.Visible;
                    }
                }
            }
        }
    }
}
